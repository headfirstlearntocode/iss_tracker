import requests, json, turtle

iss = turtle.Turtle()

#General settings of the windows and the object created
def setup(window):
    global iss
    window.setup(1000,500)  #size of windows 1000px * 500px
    window.bgpic('earth.gif')
    window.setworldcoordinates(-180,-90,180,90) #coordinates system
    turtle.register_shape('iss.gif')
    iss.shape('iss.gif')

#It displays the current position of the ISS
def move_iss(latitude, longitude):
    global iss
    iss.penup()
    iss.goto(longitude,latitude)
    iss.pendown()


#Getting the position of the ISS from the API
def track_iss():
    url = 'http://api.open-notify.org/iss-now.json'
    response = requests.get(url)

    if (response.status_code == 200):
        #print(response.text)
        response_dictionary = json.loads(response.text)
        #print(response_dictionary)
        position = response_dictionary['iss_position']
        latitude = float(position['latitude'])
        longitude = float(position['longitude'])
        move_iss(latitude,longitude)
        #print('International Space Station is at', latitude, longitude)
    else:
        print('Houston we have a problem', response.status_code)
    widget = turtle.getcanvas()
    widget.after(5000, track_iss)

def main():
    global iss
    screen = turtle.Screen()    #reference to the turtle's scree object
    setup(screen)
    track_iss()


if __name__ == "__main__":
    main()
    turtle.mainloop()